# Ukuli Work Mobile Client
Mobile client for [Ukuli Work](https://github.com/TomiToivio/ukuli-work)

## Mobile Client Branches
* Master (Distribution version, should be the same as production)
* Production (This should be merged with master)
* Staging (The one we run various tests against)
* Development (Merge the issue branches with this one)
